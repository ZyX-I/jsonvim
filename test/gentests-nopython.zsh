#!/bin/zsh
for test in *.in ; do
    cat <(<<< $':let g:json_UsePython=0\n') $test > ${test:r}-nopython.in
done
